This code implements a comparator circuit using the concept of spiking neurons. The details of the assumptions made, its usage and the modified diagram has been provided in the "Usage Guidelines and Assumptions doc.doc" along with "Modified_comparator.jpg". If you plan
to use our work in any of your publications, we would like to request you to cite our paper:

Bagchi, Samya, Srikrishna S. Bhat, and Atul Kumar. "O (1) time sorting algorithms using spiking neurons." In Neural Networks (IJCNN), 2016 International Joint Conference on, pp. 1037-1043. IEEE, 2016.