// include CARLsim user interface
#include <iostream>
#include <carlsim.h>
#include <stdlib.h>

#if defined(WIN32) || defined(WIN64)
#include <stopwatch.h>
#endif

#define SIMSECONDS 1
#define SIMMILLISECONDS 0
#define BITLIMIT 32
#define DELAY 4
#define EXCITSPIKE 30

using namespace std;

//The sign connections is used in order to excite
//a neuron representing a numberwhich would have
//been considered smaller compared to a negative
//number due to the twos-complement representation
class SignConnection: public ConnectionGenerator
{
	int xLen, yLen, zLen;
public:
	SignConnection() {}
	SignConnection(int n)
	{
		xLen = BITLIMIT;
		yLen = n;
		zLen = n;
	}
	~SignConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = 0, y1 = i/yLen, z1 = i%yLen;
		int x2 = 0, y2 = j/yLen, z2 = j%yLen;
		connected = (y2!=z2) && (y1!=z1) && (y1 == z2) && (z1 == y2);
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 6;
	}
};

//Connect the inhibitory part of signed bit neuron to
//the comparator. This part is necessary as we are
//using the twos complement form of representing
//numbers. Therefore any negative number will have
//the MSB as 1, in which case the comparison has to be
//inverted.
class SingleConnection: public ConnectionGenerator
{
	int xLen, yLen, zLen;
public:
	SingleConnection() {}
	SingleConnection(int n)
	{
		xLen = BITLIMIT;
		yLen = n;
		zLen = n;
	}
	~SingleConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = i/yLen, y1 = i%yLen, z1 = 0;
		int x2 = 0, y2 = j/yLen, z2 = j%yLen;
		connected = (x1 == 0) && (y2!=z2) && (y1!=z1) && (y1 == y2);
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 1;
	}
};

//Connection between the number and the comparison layer
//of the comparator. The connection is such that every
//neuron representing a bit of the number is connected
//to the respective neuron in the comparison layer.
class NumberCompConnection: public ConnectionGenerator
{
	int xLen, yLen, zLen;
public:
	NumberCompConnection() {};
	NumberCompConnection(int n)
	{
		xLen = BITLIMIT;
		yLen = n;
		zLen = n;
	}
	~NumberCompConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = i/yLen, y1 = i%yLen, z1 = 0;
		int x2 = (j/yLen)%xLen, y2 = j%yLen, z2 = j/(yLen * xLen);
		connected = false;
		if((x1 == x2) && ((y2!=z2) && (y1 == y2)))
			connected = true;
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 6;
	}
};

//Connection between the inhibitory number and the 
//comparison layer of the comparator. Connections are
//made such that neurons belonging to the same bit but
//different numbers are connected in order to inhibit.
class InhibCompConnection: public ConnectionGenerator
{
	int xLen, yLen, zLen;
public:
	InhibCompConnection() {};
	InhibCompConnection(int n)
	{
		xLen = BITLIMIT;
		yLen = n;
		zLen = n;
	}
	~InhibCompConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = i/yLen, y1 = i%yLen, z1 = 0;
		int x2 = (j/yLen)%xLen, y2 = j%yLen, z2 = j/(yLen * xLen);
		connected = false;
		if((x1 == x2) && ((y2!=z2) && (y1 == z2)))
		{
			connected = true;
		}
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 1;
	}
};

//Connection between the selection layer and the comparator matrix.
//All the neurons corresponding to the number i being compared to number j
//are connected to the neuron at row i and column j in the comparison matrix
class SelectCompConnection: public ConnectionGenerator
{
	int xLen, yLen, zLen;
public:
	SelectCompConnection() {};
	SelectCompConnection(int n)
	{
		xLen = BITLIMIT;
		yLen = n;
		zLen = n;
	}
	~SelectCompConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = (i/yLen)%xLen, y1 = i%yLen, z1 = i/(yLen * xLen);
		int x2 = 0, y2 = j/yLen, z2 = j%yLen;
		connected = false;
		if((y1 == y2) && (z1 == z2))
		{
			connected = true;
			//fprintf(stderr, "%d %d %d %d %d\n", x1, y1, x2, y2, z2);
		}
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 1;
	}
};

//Connection between the inhibitory comparison layer and the selection
//layer of the comparator. The every neuron representing a bit n of number
//i in the comparison layer is connected to every neuron representing
//a bit m < n of a number j in the selection layer.
class CompSelectConnection: public ConnectionGenerator
{
	int xLen, yLen, zLen;
public:
	CompSelectConnection() {};
	CompSelectConnection(int n)
	{
		xLen = BITLIMIT;
		yLen = n;
		zLen = n;
	}
	~CompSelectConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = (i/yLen)%xLen, y1 = i%yLen, z1 = i/(yLen * xLen);
		int x2 = (j/yLen)%xLen, y2 = j%yLen, z2 = j/(yLen * xLen);
		connected = false;
		if((x1 < x2) && (y2!=z2) && (y1 != z1) &&
			(((y1 == z2) && (y2 == z1)) || ((y1 == y2) && (z1 == z2))))
			connected = true;
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 1;
	}
};

//Customised spike generator class
class TemporalSpikes: public SpikeGenerator
{
	vector<vector<int> > spikeList;
	vector<int> finishList;
public:
	TemporalSpikes(){};
	TemporalSpikes(vector<vector<int> > spikes)
	{
		for(int i=0; i<spikes.size(); i++)
		{
			vector<int> temp;
			for(int j=0; j<spikes[i].size(); j++)
			{
				temp.push_back(spikes[i][j]);
				finishList.push_back(0);
			}
			spikeList.push_back(temp);
		}
	}
	unsigned int nextSpikeTime(CARLsim *sim, int grpId, int nid, unsigned int currentTime, unsigned int lastScheduledSpikeTime,
		unsigned int endOfTimeSlice)
	{
		int x=nid/spikeList.size(), y=nid%spikeList.size();
		if(finishList[nid] || (!spikeList[y][x]))
			return SIMSECONDS * 1000 + SIMMILLISECONDS + 1;
		else
		{
			finishList[nid]++;
			return spikeList[y][x];
		}
	}
};

//Converts number to spike trains
vector<vector<int> > convertNumToSpike(int n, int *array)
{
	vector<vector<int> > spikeList;
	for(int i=0; i<n; i++)
	{
		vector<int> bitList;
		int temp = BITLIMIT, num = array[i], time = 1;
		while(temp>0)
		{
			bitList.push_back(num & 1);
			num = num >> 1;
			temp--;
		}
		for(int i=0; i<bitList.size()/2; i++)
		{
			int temp = bitList[i];
			bitList[i] = bitList[bitList.size() - i - 1];
			bitList[bitList.size() - i - 1] = temp;
		}
		spikeList.push_back(bitList);
	}
	return spikeList;
}

int main(int argc, char **argv) {

	const int n = 2;
	
	//Parameters for initilaising the simulator
	//Note: ithGPU isn't needed if the simulator is running in CPU mode
	//Also note from the CARLsim documentation that except for the first argument
	//all the other arguments have a default argument
	int ithGPU = 0;
	int randSeed = 42;
	CARLsim sim("Comparator",CPU_MODE,USER,ithGPU,randSeed);

	int numList, inhibNumList;
	int compLayer, inhibCompLayer;
	int selectionLayer;
	int execInter, delayExecInter, inhibInter;
	int comparator, inhib_comparator, pre_rank, inhib_pre_rank;
	int rank, selection, numbers[n];
	int inter_spike;
	for(int i=0; i<n; i++)
		if(!scanf("%d", &numbers[i]))
			return 1;

	//Declare the connection groups for connecting neuron groups in the future
	NumberCompConnection numCompConn(n);
	InhibCompConnection inhibCompConn(n);
	CompSelectConnection compSelectConn(n);
	SelectCompConnection selectCompConn(n);
	SingleConnection singleConn(n);
	SignConnection signConn(n);

	//Create the required neuron groups
	numList = sim.createSpikeGeneratorGroup("Number", BITLIMIT * n, EXCITATORY_NEURON);
	inhibNumList = sim.createGroup("Inhibitory Number", BITLIMIT * n, INHIBITORY_NEURON);
	comparator = sim.createGroup("Comparator-Matrix", n * n, EXCITATORY_NEURON);
	inhib_comparator = sim.createGroup("Comparator-Matrix", n * n, INHIBITORY_NEURON);
	execInter = sim.createGroup("excitatory neuron", n*n, EXCITATORY_NEURON);
	delayExecInter = sim.createGroup("excitatory-delayor neuron", n*n, EXCITATORY_NEURON);
	inhibInter = sim.createGroup("inhibitory-bit neuron", n*n, INHIBITORY_NEURON);

	//selection = sim.createGroup("Selection-Layer " + rowstream.str(), n, EXCITATORY_NEURON);
	compLayer = sim.createGroup("Comparator Layer", BITLIMIT * n * n, EXCITATORY_NEURON);
	inhibCompLayer = sim.createGroup("Inhibitory Comp Layer", BITLIMIT * n * n, INHIBITORY_NEURON);
	selectionLayer = sim.createGroup("Selection Matrix", BITLIMIT * n * n, EXCITATORY_NEURON);

	//set the parameters for the neurons
	sim.setNeuronParameters(inhibNumList, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(comparator, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(inhib_comparator, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(compLayer, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(inhibCompLayer, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(selectionLayer, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(execInter, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(delayExecInter, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(inhibInter, 0.02f, 0.2f, -65.0f, 8.0f);

	//connect numList with inhibitory numlist
	sim.connect(numList, inhibNumList, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);

	//connect number and comparison layer
	//The extra delay neurons is for synchronization
	//of spikes of the inhibitory layers and the excitatory
	//layers. CARLsim needs you to specify inhibitory layers
	//instead of synapses due to which extra inhibitory
	//layers were needed.
	sim.connect(numList, compLayer, &numCompConn, SYN_FIXED);
	sim.connect(inhibNumList, compLayer, &inhibCompConn, SYN_FIXED);
	sim.connect(numList, execInter, &singleConn, SYN_FIXED);
	sim.connect(execInter, delayExecInter, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(6), RadiusRF(-1), SYN_FIXED);
	sim.connect(execInter, inhibInter, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(6), RadiusRF(-1), SYN_FIXED);
	sim.connect(inhibInter, comparator, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);
	sim.connect(delayExecInter, comparator, &signConn, SYN_FIXED);

	//connect comparison layer and selection layer
	sim.connect(compLayer, inhibCompLayer, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);
	sim.connect(compLayer, selectionLayer, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(6), RadiusRF(-1), SYN_FIXED);
	sim.connect(inhibCompLayer, selectionLayer, &compSelectConn, SYN_FIXED);

	//connect selection layer and comparator matrix
	sim.connect(selectionLayer, comparator, &selectCompConn, SYN_FIXED);
	
	//connect comparator and inhibitory comparator.
	//This is done to prevent extra spikes from the comparator
	sim.connect(comparator, inhib_comparator, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);
	sim.connect(inhib_comparator, comparator, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);

	//This loop is responsible for connecting all the gate neurons in a row to the
	//corresponding adder neuron in the adder matrix.
	sim.setConductances(true);

	//convert input numbers to spikes
	vector<vector<int> > testSpike;
	testSpike = convertNumToSpike(n, numbers);
	TemporalSpikes excSpike;
	excSpike = TemporalSpikes(testSpike);
	sim.setSpikeGenerator(numList, &excSpike);

	//setup the neurons, the connections and the generators.
	sim.setupNetwork();

	// set some spike monitors
	SpikeMonitor *spkMonout;
	spkMonout = sim.setSpikeMonitor(comparator, "DEFAULT");

	// run for 1 second
	// at the end of runNetwork call, SpikeMonitor stats will be printed
	spkMonout->startRecording();
	cout<<"Finished Recording\n";
	sim.runNetwork(SIMSECONDS, SIMMILLISECONDS);
	spkMonout->stopRecording();
	printf("Selection\n");
	vector<vector<int> > output = spkMonout->getSpikeVector2D();
	for(int i=0; i<output.size(); i++)
	{
		if(i%n==0)
			fprintf(stderr, "\n");
		vector<int> spikeTime = output[i];
	//	fprintf(stderr, "%d %d %d ", (i/n)%BITLIMIT, i%n, i/(BITLIMIT*n));
		for(int j=0; j<spikeTime.size(); j++)
			fprintf(stderr, "%d ", spikeTime[j]);
			//fprintf(stderr, "1 ");
		if(!spikeTime.size())
			fprintf(stderr, "0 ");
	//	fprintf(stderr, "\n");
	}
	fprintf(stderr, "\n");
	return 0;
}
